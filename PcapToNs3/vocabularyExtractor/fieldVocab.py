#!/usr/bin/python
#Creates a file containing packet type sequences from an input field data file
import sys
from lxml import etree
from bs4 import BeautifulSoup
from collections import OrderedDict

if len(sys.argv) != 3:
	print "usage: fieldVocab.py <input file> <model name>"
	print "<input file>"
	print " -a file containing a packet trace containing packet type attributes\n"
	print "<model name>"
	print " -the name of the model that is being generated\n"
	print "This program creates one file: "
	print "modelStandardizedXMLFile.xml"
	print " -this file contains field vocabularies for each packet type"
	sys.exit(-1) 
inputPacketsTypesFile = sys.argv[1]
modelName = sys.argv[2]
outputFilename = modelName+'/modelStandardizedXMLFile.xml'
packetTypeAppearances = 0
#we want to keep track of fields in order
fields = OrderedDict()

#Create the root element for the output file:
modelXML = etree.Element(modelName)

#---------------process the input file------------------#
soup = BeautifulSoup(open(inputPacketsTypesFile,'r'), 'xml')
#get all of the unique packet types:
setPacketTypes = set()
for packetType in soup.find_all('packet'):
	try:
		setPacketTypes.add( packetType['type'])
	except:
		continue
#for each unique packet type
for packetType in setPacketTypes:
	#iterate through each packet per type:
	fields.clear()
	packetTypeAppearances = 0
	for packetXML in soup.find_all('packet',type=packetType):
		packetTypeAppearances = packetTypeAppearances+1
		#iterate through each field in the packet
		for fieldXML in packetXML.find_all('field'):
			#store the unique values in all fields
			#The resulting structure is as follows:
			# field dictionary contains fieldnames dictionary
			#  fieldnames dictionary contains fieldvalues list
			#an example reference is the following:
			#fields['icmp.code']['size'] 
			# this will print out a list of all of the values in the size field
			for child in fieldXML.find_all(True, recursive=False):
				#print child
				
			
				if fieldXML.mname.contents[0].strip() not in fields:
					print fieldXML.mname.contents[0].strip()
					fields[fieldXML.mname.contents[0].strip()] = {}
				if child.name not in fields[fieldXML.mname.contents[0].strip()]:
					print child.name
					fields[fieldXML.mname.contents[0].strip()][child.name] = []
				try:
					if child.contents[0].strip() not in fields[fieldXML.mname.contents[0].strip()][child.name]:
						fields[fieldXML.mname.contents[0].strip()][child.name].append(child.contents[0].strip())
				except: 
					continue
	#-----------process the field entropy and mvocab-------#
	#create the types element and include all of the attributes that make it a unique type:
	typeXML = etree.SubElement(modelXML, "mtype", id=packetXML['type'], 
	nodeuniq=packetXML['nodeuniq'],
	typeuniq=packetXML['typeuniq'])
	#add all of the subelements that belong to this packet type
	for fieldName in fields:
		fieldXML = etree.SubElement(typeXML, "mfield") 
		for metaFieldname in fields[fieldName]:
			metaFieldXML = etree.SubElement(fieldXML, metaFieldname)
			metaFieldXML.text = ""
			for vocabItem in fields[fieldName][metaFieldname]:
				metaFieldXML.text = str(metaFieldXML.text) + str(vocabItem)+";"
		#THIS IS WHERE I COULD CALCULATE OTHER stats for the fields
		#calculate the 'value' vocabulary entropy		
		numberOfDifferentValues = len(fields[fieldName]['mvalue']) if len(fields[fieldName]['mvalue']) != 1 else 0
		etree.SubElement(fieldXML, "mentropy").text = str(numberOfDifferentValues/(packetTypeAppearances*1.0))
	#add an attribute containing the number of times this packet type was observed
	typeXML.set('appears',str(packetTypeAppearances))

#output the xml tree	
print etree.tostring(modelXML,pretty_print='true')

out = open(outputFilename, 'w')
out.write(etree.tostring(modelXML,pretty_print='true'))
