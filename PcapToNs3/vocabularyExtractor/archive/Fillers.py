class Fillers:
    showname = None
    size= None
    show = None
    pos = None
    name = None
    entropy=None

    def __init__(self):
        self.vocab = []

    def constructFiller(self, showname, size, show, pos, name):
        self.showname = showname
        self.size = size
        self.show = show
        self.pos = pos
        self.name = name

    def addEntropy(self, entropy):
        self.entropy = entropy
