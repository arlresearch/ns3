import sys


from Type import Type
from Fillers import Fillers

file = str(sys.argv[1])
file2 = str(sys.argv[2])
dirName = str(sys.argv[3])
typesFile= open(file, "r")
fieldsFile = open(file2, "r")

def seperate(line):
    left, line = line.split(":",1)
    left = left.strip()
    line = line.strip()
    if line != None:
        return line
    else:
        return left

def getShowname(line):
    showname, right = line.split("#", 1)
    return showname

def output(obj):
    for each in obj.fields:
        print each

def stripAll(str):
    field, right = str.split("#")


    pos = str.rfind('#')
    if pos != None:
        str = str[:pos]

    str=str.rstrip('\n')+" "
    #print "in strip method: "+str+ "\n"
    return str
def rmColon(str):
    pos = str.rfind(':')
    if pos != None:
        str = str[:pos]
    str.strip()
    return str

allTypes = []
eachType = []
typeObjArray = []
line = typesFile.readline()

allTypes = map(str, line.split(' '))

for type in allTypes:
    if type in eachType:
        continue
    else:
        eachType.append(type)
for each in eachType:
    type = Type()
    type.addName(each)
    typeObjArray.append(type)

print len(typeObjArray)-1
print len(allTypes)-1
for each in typeObjArray:
    print each.typeName
count = 0
fieldCount = 0  #keeps track of the filler field
position = 0
for line in fieldsFile:
    spaceFound = False
#       print line
    if not line.strip():
        #print "Space Found #############################################################"
        #print "count is " + str(count)
        count += 1
        fieldCount = 0
        typeObjArray[position].spaceFound = True
        continue

    else:
        showname = getShowname(line)
        if count < len(allTypes):

            currType = allTypes[count]
            #print "currTYpe = "+currType
        else:
            break

        index = 0
        for each in typeObjArray:
            if each.typeName == currType:
                break
            else:
                index += 1
        position = index
        #if spaceFound == True:
            #typeObjArray[index].spaceFound = spaceFound
        #print spaceFound
        #print "This is the index here####################: "+str(index)
        try:
            left, right = line.split(":", 1)
            left=left.rstrip('\n')+" "
            ###add to the field array in the pertaining type object####
            print "this is left : "+left
            #if left not in typeObjArray[index].fields:
            if typeObjArray[index].spaceFound != True:
                typeObjArray[index].fields.append(left)
                filler = Fillers()
                typeObjArray[index].fillers.append(filler)
                #typeObjArray[index].spaceFound = True

            print "this is what is left in the right####################################"+right
            blank, size, show, pos, value, name = right.split('#')
            #size = seperate(size)
            size = seperate(size)
            show = seperate(show)
            pos = seperate(pos)
            value = seperate(value)
            name = seperate(name)
            name=name.rstrip('\n')
            showname = rmColon(showname)
            ############################fix here################################3

            #print "right values : ##### "+showname+name + size + value
            print "length of filler array: "+str(len(typeObjArray[index].fillers))
            print "length of fieldCount index: "+str(fieldCount)
            fillerPos = typeObjArray[index].fillers[fieldCount]

            if fillerPos.name == None:
                fillerPos.constructFiller(showname, size, show, pos, name)
            if value not in fillerPos.vocab:
                fillerPos.vocab.append(value)
        #       if right != "":
 #              right=stripAll(right)
            print "This is the field name"+fillerPos.name
        except ValueError:
            left = line

        fieldCount += 1 #test tabbed it, if breaks then untab once

for each in typeObjArray:
    print "IN type "+ each.typeName
    if len(each.fillers)>0:
        for each2 in each.fillers:
            vocLength = len(each2.vocab)
            if vocLength > 0:
                print "   vocab length:" +str(vocLength)
                entropy = (100-(100/vocLength))/100.0
                print "   entropy should be: " +str(entropy)
            else:
                entropy = 0.0
            each2.addEntropy(entropy)

for each in typeObjArray:
    print len(each.fields)
    print "length of filler: "+str(len(each.fillers))
    output(each)
    print "###############End of Fields position"

print "Printing the values in fillers of types: ###########################"
xmlFile = open(dirName+"/modelStandardizedXMLFile.xml", "w+")
xmlFile.write("<summerping>\n")
for each in typeObjArray:
    #if len(each.fields)>0:
    if each.typeName != "":
        counting = 0
        xmlFile.write("   <mtype id = '"+ each.typeName+"'>\n")
        #xmlFile.write("      <mfield>\n")
        for each2 in each.fields:
            xmlFile.write("      <mfield>\n")
            each3 = each.fillers[counting]
            xmlFile.write("         <mname>"+each3.name+"</mname>\n")
            xmlFile.write("         <mshowname>"+each3.showname+"</mshowname>\n")
            xmlFile.write("         <msize>"+str(each3.size)+"</msize>\n")
            xmlFile.write("         <mpos>"+str(each3.pos)+"</mpos>\n")
            xmlFile.write("         <mshow>"+each3.show+"</mshow>\n")
            xmlFile.write("         <mvocab>")
            for each4 in each3.vocab:
                xmlFile.write(str(each4)+";")
            xmlFile.write("</mvocab>\n")
            #xmlFile.write("      <mvocab>"+each3.vocab+"</mvocab>\n")
            xmlFile.write("         <mentropy>"+str(each3.entropy)+"</mentropy>\n")
            xmlFile.write("      </mfield>\n")
            #print "   Field Name: "+ each2
            #each3 = each.fillers[counting]
            #print "     "+each3.name
            #print "     "+ str(each3.size)
            #print "     "+str(each3.pos)
            #print "     showname: "+each3.showname
            #for each4 in each3.vocab:
            #       print "       Vocabulary "+str(each4)
            counting += 1
        xmlFile.write("   </mtype>\n")
xmlFile.write("</summerping>\n")
