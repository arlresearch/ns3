import sys
fileName = str(sys.argv[1])

dotFile = open(fileName, "r")
dir = str(sys.argv[2])
statesArray = []
transArray = []
for line in dotFile:
    if "->" in line:
        #print "found ->"
        lname, rname = line.split("->")
        rname, tran = rname.split("[")
        tran = tran[tran.index('"') + 1:tran.rindex('"')]
        tuple = (lname, rname, tran)
        transArray.append(tuple)
        print "Node "+lname+" -> "+rname+" with "+tran
    else:
        try:
            nodeName, right = line.split("[")
            statesArray.append(nodeName)
            print nodeName

        except ValueError:
            continue

cppFile = open(dir+"/"+dir+"Grammar.cc", "w+")
cHeaders = """
#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>

using namespace std;\n
"""


cEnums = "enum stateName {"
i = 0
for each in statesArray:
    if i<(len(statesArray)-1):
        cEnums += each +"= "+ str(i)+", "
    else:
        cEnums += each +"= "+ str(i)
    i+= 1
cEnums += "};"

cHeaders += cEnums
cppFile.write(cHeaders)

transitionClass = """
class Transition{
        int transNum;
        stateName state;

public:
        void addState(stateName newState){
                state = newState;
                }
        void addTransNum(int nextNum){
                transNum = nextNum;

                }
        stateName getName(){
                return state;
                }
        int getNum(){
                return transNum;
                }

        };
\n
"""
stateClass = """
class State{

public:

        vector<Transition> trans;
        void addTrans(int input,stateName stateNumber){
                Transition temp;
                temp.addState(stateNumber);
                temp.addTransNum(input);
                trans.push_back(temp);
                }

        };\n
"""
body = transitionClass + stateClass
cppFile.write(body)

stateMachineClass = """
class StateMachine{
        State stateArray["""+str(len(statesArray))+"""];
        int currState = 0;

public:
        StateMachine(){
        """

for each in transArray:
    stateMachineClass+="stateArray["+each[0].strip()+"].addTrans("+each[2]+", "+each[1].strip()+");\n        "
   #stateArray[node0].addTrans(1,node2);
#stateArray[node1].addTrans(1,node0);
# stateArray[node1].addTrans(0,node2);

   #stateArray[node2].addTrans(1,node1);
stateMachineClass+= """
                }

        vector<int> getNextState(int input){
                int found = 0;
                printf("Input is:%i ", input);
                int transitNum;
                vector<int> result;
                //printf("Current size is: %i", stateArray[currState].trans.size());
                for(int i = 0; i< stateArray[currState].trans.size(); i++){
                        stateName state = stateArray[currState].trans[i].getName();
                        transitNum = stateArray[currState].trans[i].getNum();
                        //printf("%s%i","trans is: ",transitNum);
                        if(transitNum == input){
                                //result.push_back(transitNum);
                                currState = state;
                                //printf("CurrState changed: %i", currState);
                                found = 1;
                                break;
                        }
                }
                if(found){
                        for(int i = 0; i< stateArray[currState].trans.size(); i++){
                                transitNum = stateArray[currState].trans[i].getNum();
                                result.push_back(transitNum);
                        //printf("%s%i","trans is: ",transitNum);
                        }
                        //printf("%s%i","trans is: ",transitNum);
                }
                else{
                //if(result.size()==0){
                        result.push_back(-1);
                        //}
                        }
                 return result;
                }
};
"""

cppFile.write(stateMachineClass)
printV = """
void printV(vector<int> out){
        if(out.size()==1){
                printf("transition is: ");
        }
        else{
                printf("transitions are: ");
        }
        for(int i = 0; i < out.size(); i++){
                printf("%i ",out[i]);
        }
        printf("\\n");

}\n
"""
cppFile.write(printV)
