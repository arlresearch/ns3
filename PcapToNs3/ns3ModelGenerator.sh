#!/bin/bash
# ------------------------------------------------------------------
# Summer 2015
#	-Jaime Acosta
#	-Felipe Sotelo
#	-Caesar Zapata
#          
#	This script will generate an ns3 scenario file and a ping model (including helper files)
#
# 2015-08-11:
# 	Script created and tested with the following parameters: 
#	./ns3ModelGenerator.sh  samples/captures/ping_capture_with_eth_fcs.pcap icmp summerping"
#
# ------------------------------------------------------------------

SUBJECT=some-unique-id
VERSION=0.1.0
USAGE="Usage: ./ns3ModelGenerator.sh <pcap file> <protocol> <model name>"

# --- Option processing --------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi

while getopts ":vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo $USAGE
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $(($OPTIND - 1))

if [ $# -ne 3 ] ; then
    echo $USAGE
    exit 1;
fi

PCAPFILE=$1
PROTOCOL=$2
MODELNAME=$3 
#Convert the model name to all lowercase (an ns3 thing)
MODELNAME=${MODELNAME,,}

#we only want one instance of this script running at one time
LOCK_FILE=/tmp/${SUBJECT}.lock
if [ -f "$LOCK_FILE" ]; then
echo "Script is already running"
exit
fi
trap "rm -f $LOCK_FILE" EXIT
touch $LOCK_FILE 

# -----------------------------------------------------------------
#only uncomment the following lines the first time you execute this script
tar jxvf ns-allinone-3.23.tar.bz2
cp modifiedNS3Files/ipv4-address-helper.* ns-allinone-3.23/ns-3.23/src/internet/helper/
#------------------------------------------------------------------

#First extract fields and flows from the pcap file (first converted to pdml with tshark)
python pdmlExtractor/pdmlExtractor.py $PCAPFILE $PROTOCOL $MODELNAME

#Get the types of messages and produce the sequence for prospex (will create a state machine)
python packetTypeExtractor/packetTypeExtractor.py $MODELNAME/packets.xml $MODELNAME

#Create the model Standardized file
python vocabularyExtractor/fieldVocab.py $MODELNAME/packetsTypes.xml $MODELNAME
mkdir $MODELNAME/$MODELNAME"sm"/
cp $MODELNAME/packetTypeSequences.txt $MODELNAME/$MODELNAME"sm"/sessions.txt
cd prospex 
python prospex.py ../$MODELNAME/$MODELNAME"sm"
cd ../
python grammarGen/dotToCppGrammer.py $MODELNAME/$MODELNAME"sm"/labeledstatemachine.dot $MODELNAME

cd ns-allinone-3.23/ns-3.23/src/
./create-module.py $MODELNAME
cd ../../../

echo `pwd`

#Create the model files and place them in the directory created by ./create-module.py
python modelFileGen/ns3ModelGenerator_hFile.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/model/"$MODELNAME".h
python modelFileGen/ns3ModelGenerator_ccFile.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/model/"$MODELNAME".cc
python modelFileGen/ns3ModelGenerator_hFileHelper.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/helper/"$MODELNAME"-helper.h
python modelFileGen/ns3ModelGenerator_ccFileHelper.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/helper/"$MODELNAME"-helper.cc

#now for the type files:
python modelFileGen/ns3ModelGenerator_hFileTypes.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/model/"$MODELNAME"-type.h
python modelFileGen/ns3ModelGenerator_ccFileTypes.py $MODELNAME/modelStandardizedXMLFile.xml $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/model/"$MODELNAME"-type.cc

#now copy a new wscript file that contains references to the needed code files
python modelFileGen/ns3WscriptGenerator.py $MODELNAME > ns-allinone-3.23/ns-3.23/src/"$MODELNAME"/wscript

#create the scenario generator:
cp scenarioGen/xmlToNs3Scenario.py  $MODELNAME/
cd $MODELNAME
./xmlToNs3Scenario.py $MODELNAME

#run the simulation
cp $MODELNAME".cc" ../ns-allinone-3.23/ns-3.23/scratch
cd ../ns-allinone-3.23/ns-3.23/
./waf configure
make
./waf --run scratch/$MODELNAME

#copy the resulting pcap files
cd ../../
mkdir pcapCaptures$MODELNAME
cp ns-allinone-3.23/ns-3.23/*.pcap pcapCaptures$MODELNAME
