#!/usr/bin/python
#Creates a C++ NS-3 WScript file

import sys
from jinja2 import Template

modelName = sys.argv[1]

wscriptFile = Template('''
# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# def options(opt):
#     pass

# def configure(conf):
#     conf.check_nonfatal(header_name='stdint.h', define_name='HAVE_STDINT_H')

def build(bld):
    module = bld.create_ns3_module('{{jinjaModelName}}', ['core'])
    module.source = [
        'model/{{jinjaModelName}}.cc',
        'model/{{jinjaModelName}}-type.cc',
        'helper/{{jinjaModelName}}-helper.cc',
        ]

    module_test = bld.create_ns3_module_test_library('{{jinjaModelName}}')
    module_test.source = [
        'test/{{jinjaModelName}}-test-suite.cc',
        ]

    headers = bld(features='ns3header')
    headers.module = '{{jinjaModelName}}'
    headers.source = [
        'model/{{jinjaModelName}}.h',
        'model/{{jinjaModelName}}-type.h',
        'helper/{{jinjaModelName}}-helper.h',
        ]

    if bld.env.ENABLE_EXAMPLES:
        bld.recurse('examples')

    # bld.ns3_python_bindings()

''')
print wscriptFile.render(jinjaModelName=modelName, todo='TODO')
