#!/usr/bin/python
#Creates a C++ NS-3 Model Using python
from bs4 import BeautifulSoup
from jinja2 import Template
import sys

xmlFilename = sys.argv[1]
modelName = sys.argv[2]

soup = BeautifulSoup(open(xmlFilename,'r'), 'xml')

myFields = []
currFieldPos = 0
totalPacketSize = 0

for myType in soup.find_all('mtype',id='0'):
	currPacketSize = 0
	for myField in myType.find_all('mfield'):
	#only include the field if its byte location has not been specified by another field:
		if myField.msize.contents[0].split(';')[0]!='unspecified' and myField.mpos.contents[0].split(';')[0] > currFieldPos:
			totalPacketSize += int(myField.msize.contents[0].split(';')[0])
			#fix up the mname field:
			mname = myField.mname.contents[0].split(';')[0].replace("(","").replace(")","").replace(" ","_").replace(".","_")
			#fix up the vocab field to work with C++ (hex bytes)
			mvalue = ""
	#               for mvalueItem in myField.mvalue.contents[0].split(';'):
			mvalueItem = myField.mvalue.contents[0].split(';')[0]
			if mvalueItem.strip() != ';' and mvalueItem.strip() != '':
				if myField.msize.contents[0].split(';')[0] == '1' or myField.msize.contents[0].split(';')[0] == '2' or myField.msize.contents[0].split(';')[0] == '4' or myField.msize.contents[0].split(';')[0] == '8':
					mvalue="0x"+mvalueItem
				else:
					mvalueItem = mvalueItem.decode("hex")
					mvalue=''.join( [ "0x%02X " % ord( x ) for x in mvalueItem ] ).strip()
			myFields.append((myType['id'],mname, myField.msize.contents[0].split(';')[0], myField.mentropy.contents[0].split(';')[0], mvalue, currPacketSize,myField.mpos.contents[0].split(';')[0]))
			currFieldPos = myField.mpos.contents[0].split(';')[0]+myField.msize.contents[0].split(';')[0]

ccFile = Template('''
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "{{jinjaModelName}}.h"
#include "{{jinjaModelName}}-type.h"
#include "ns3/header.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-header.h"
#include <stdint.h>
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/socket.h"
#include "ns3/uinteger.h"
#include "ns3/boolean.h"
#include "ns3/inet-socket-address.h"
#include "ns3/packet.h"
#include "ns3/trace-source-accessor.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("{{jinjaModelName}}");

NS_OBJECT_ENSURE_REGISTERED ({{jinjaModelName}});

TypeId
{{jinjaModelName}}::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::{{jinjaModelName}}")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<{{jinjaModelName}}> ()
    .AddAttribute ("Remote",
                   "The address of the machine we want to send a {{jinjaModelName}} packet to.",
                   Ipv4AddressValue (),
                   MakeIpv4AddressAccessor (&{{jinjaModelName}}::m_remote),
                   MakeIpv4AddressChecker ())
    .AddAttribute ("Verbose",
                   "Produce usual output.",
                   BooleanValue (false),
                   MakeBooleanAccessor (&{{jinjaModelName}}::m_verbose),
                   MakeBooleanChecker ())
    .AddAttribute ("Interval", "Wait  interval  seconds between sending each packet.",
                   TimeValue (Seconds (1)),
                   MakeTimeAccessor (&{{jinjaModelName}}::m_interval),
                   MakeTimeChecker ())
    .AddAttribute ("Size", "The number of data bytes to be sent.",
                   UintegerValue (56),
                   MakeUintegerAccessor (&{{jinjaModelName}}::m_size),
                   MakeUintegerChecker<uint32_t> (16))
    .AddTraceSource ("Rtt",
                     "The rtt calculated by the ping.",
                     MakeTraceSourceAccessor (&{{jinjaModelName}}::m_traceRtt),
                     "ns3::Time::TracedCallback");
  ;
  return tid;
}

{{jinjaModelName}}::{{jinjaModelName}} ()
  : m_interval (Seconds (1)),
    m_size ({{jinjaPacketSize}}),
    m_socket (0),
    m_seq (0),
    m_verbose (false),
    m_recv (0)
{

  NS_LOG_FUNCTION (this);
}
{{jinjaModelName}}::~{{jinjaModelName}} ()
{
  NS_LOG_FUNCTION (this);
}

void
{{jinjaModelName}}::DoDispose (void)
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;
  Application::DoDispose ();
}

uint32_t
{{jinjaModelName}}::GetApplicationId (void) const
{
  NS_LOG_FUNCTION (this);
  Ptr<Node> node = GetNode ();
  for (uint32_t i = 0; i < node->GetNApplications (); ++i)
    {
      if (node->GetApplication (i) == this)
        {
          return i;
        }
    }
  NS_ASSERT_MSG (false, "forgot to add application to node");
  return 0; // quiet compiler
}

void
{{jinjaModelName}}::Receive (Ptr<Socket> socket)
{
  NS_LOG_FUNCTION (this << socket);
  while (m_socket->GetRxAvailable () > 0)
    {
      Address from;
      Ptr<Packet> p = m_socket->RecvFrom (0xffffffff, 0, from);
      NS_LOG_DEBUG ("recv " << p->GetSize () << " bytes");
      NS_ASSERT (InetSocketAddress::IsMatchingType (from));
      InetSocketAddress realFrom = InetSocketAddress::ConvertFrom (from);
      NS_ASSERT (realFrom.GetPort () == 1); // protocol should be icmp.
      Ipv4Header ipv4;
      p->RemoveHeader (ipv4);
      uint32_t recvSize = p->GetSize ();
      unsigned char data[p->GetSize()];
      p->CopyData(data,p->GetSize());
      {{jinjaModelName}}Type myType = {{jinjaModelName}}Type();
      myType.isPacketType(data, p->GetSize());
      std::cout << "received " << recvSize << " payload bytes" << std::endl;
      //NS_ASSERT (ipv4.GetProtocol () == 1); // protocol should be icmp.
      //Icmpv4Header icmp;
      //p->RemoveHeader (icmp);
      //if (icmp.GetType () == Icmpv4Header::ECHO_REPLY){}
      
     }
}

void
{{jinjaModelName}}::Send ()
{
  NS_LOG_FUNCTION (this);
  //call a type constructor:
  {{jinjaModelName}}Type myType = {{jinjaModelName}}Type();
  Ptr<Packet> dataPacket = myType.getPacket();

  m_socket->Send (dataPacket, 0);
  m_next = Simulator::Schedule (m_interval, &{{jinjaModelName}}::Send, this);
}

void
{{jinjaModelName}}::StartApplication (void)
{
  NS_LOG_FUNCTION (this);

  m_started = Simulator::Now ();
  if (m_verbose)
    {
      std::cout << "{{jinjaModelName}}  " << m_remote << " 56(84) bytes of data.\\n";
    }

  m_socket = Socket::CreateSocket (GetNode (), TypeId::LookupByName ("ns3::Ipv4RawSocketFactory"));
  NS_ASSERT (m_socket != 0);
  m_socket->SetAttribute ("Protocol", UintegerValue (1)); // icmp
  m_socket->SetRecvCallback (MakeCallback (&{{jinjaModelName}}::Receive, this));
  InetSocketAddress src = InetSocketAddress (Ipv4Address::GetAny (), 0);
  int status;
  status = m_socket->Bind (src);
  NS_ASSERT (status != -1);
  InetSocketAddress dst = InetSocketAddress (m_remote, 0);
  status = m_socket->Connect (dst);
  NS_ASSERT (status != -1);

  Send ();
}
void
{{jinjaModelName}}::StopApplication (void)
{
  NS_LOG_FUNCTION (this);
  m_next.Cancel ();
  m_socket->Close ();

  if (m_verbose)
    {

    }
}


} // namespace ns3
''')

print ccFile.render(jinjaModelName=modelName, jinjaFieldNames=myFields, todo='TODO', defaultDataType='char', jinjaPacketSize=totalPacketSize)
