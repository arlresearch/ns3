#!/usr/bin/python
#Creates a C++ NS-3 Model Using python
from bs4 import BeautifulSoup
from jinja2 import Template
import sys

xmlFilename = sys.argv[1]
modelName = sys.argv[2]

soup = BeautifulSoup(open(xmlFilename,'r'), 'xml')

myFields = []
currFieldPos = 0

for myType in soup.find_all('mtype',id='0'):
	for myField in myType.find_all('mfield'):
	#       print myType['id'],myField.mname.contents[0], myField.msize.contents[0], myField.mentropy.contents[0], myField.mvalue.contents[0]
	#only include the field if its byte location has not been specified by another field:
		if myField.msize.contents[0].split(';')[0]!='unspecified' and myField.mpos.contents[0].split(';')[0] > currFieldPos:
			mname = myField.mname.contents[0].split(';')[0].replace("(","").replace(")","").replace(" ","_").replace(".","_")
			myFields.append((myType['id'],mname.split(';')[0], myField.msize.contents[0].split(';')[0], myField.mentropy.contents[0].split(';')[0], myField.mvalue.contents[0].split(';')[0]))
			currFieldPos = myField.mpos.contents[0].split(';')[0]+myField.msize.contents[0].split(';')[0]
	#print myFields

hFile = Template('''
#ifndef {{jinjaModelName|upper}}_H
#define {{jinjaModelName|upper}}_H

#include "ns3/application.h"
#include "ns3/traced-callback.h"
#include "ns3/nstime.h"
#include "ns3/average.h"
#include "ns3/simulator.h"
#include <map>
//now include the packet types associated with this model:
#include "ns3/{{jinjaModelName}}-type{{jinjaPacketType}}.h"

namespace ns3 {

class Socket;

/**
 * \\ingroup applications
 * \\defgroup {{jinjaModelName}} {{jinjaModelName}}
 */

/**
 * \\ingroup {{jinjaModelName}}
 * \\brief {{jinjaTodo}}
 *
 * Note: {{jinjaTodo}}
 */
class {{jinjaModelName}} : public Application
{
public:
  /**
   * \\brief Get the type ID.
   * \\return the object TypeId
   */
  static TypeId GetTypeId (void);

  /**
   * create a {{jinjaTodo}}
   */
  {{jinjaModelName}} ();
  virtual ~{{jinjaModelName}} ();

private:

  // inherited from Application base class.
  virtual void StartApplication (void);
  virtual void StopApplication (void);
  virtual void DoDispose (void);
  /**
   * \\brief Return the application ID in the node.
   * \\returns the application id
   */
  uint32_t GetApplicationId (void) const;
  /**
   * \\brief Receive an {{todo}}
   * \\param socket the receiving socket
   *
   * This function is called by lower layers through a callback.
   */
  void Receive (Ptr<Socket> socket);
  /**
   * \\brief {{jinjaTodo}}
   */
  void Send ();

  /// Remote address
  Ipv4Address m_remote;
  /// Wait  interval seconds between sending each packet
  Time m_interval;
  /**
   * Specifies  the number of data bytes to be sent.
   */
  uint32_t m_size;
  /// The socket we send packets from
  Ptr<Socket> m_socket;
  /// ICMP ECHO sequence number
  uint16_t m_seq;
  /// TracedCallback for RTT measured by ICMP ECHOs
  TracedCallback<Time> m_traceRtt;
  /// produce extended output if true
  bool m_verbose;
  /// received packets counter
  uint32_t m_recv;
  /// Start time (when packet was sent)
  Time m_started;
  /// Average rtt is ms
  Average<double> m_avgRtt;
  /// Next packet will be sent
  EventId m_next;
  /// All sent but not answered packets. Map icmp seqno -> when sent
  std::map<uint16_t, Time> m_sent;
};

} // namespace ns3

#endif /* {{jinjaModelName|upper}}_H */
''')

print hFile.render(jinjaModelName=modelName, jinjaFieldNames=myFields, todo='TODO', defaultDataType='char') #'UNKNOWN_DataType')
