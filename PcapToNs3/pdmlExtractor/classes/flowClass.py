class Flow:
    protocol = None
    macsr= None
    macdst=None
    ipsr= None
    ipds=None
    time = None

    def __init__(self, protocol, macsr, macds):
        self.protocol = protocol
        self.macsr = macsr
        self.macdst = macds


    def addIpSrc(self, ipsr):
        self.ipsr = ipsr

    def addIpDst(self, ipds):
        self.ipds = ipds

    def addTime(self, time):
        self.time = time

    def __str__(self):
        if self.ipsr:
            return "Protocol: %s\n Mac Source: %s\n Mac Destination: %s\n IP Source: %s\n IP Destination: %s\n Time: %s\n" % (self.protocol, self.macsr, self.macdst, self.ipsr, self.ipds, self.time)
        else:
            return "Protocol: %s\n Mac Source: %s\n Mac Destination: %s\n" % (self.protocol, self.macsr, self.macdst)
    def __eq__(self, other):
        return self.__dict__==other.__dict__
