import sys
fileName = str(sys.argv[1])

dotFile = open(fileName, "r")

statesArray = []
transArray = []
for line in dotFile:
    if "->" in line:
        #print "found ->"
        lname, rname = line.split("->")
        rname, tran = rname.split("[")
        tran = tran[tran.index('"') + 1:tran.rindex('"')]
        tuple = (lname, rname, tran)
        transArray.append(tuple)
        print "Node "+lname+" -> "+rname+" with "+tran
    else:
        try:
            nodeName, right = line.split("[")
            statesArray.append(nodeName)
            print nodeName

        except ValueError:
            continue
