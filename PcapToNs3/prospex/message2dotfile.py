import cPickle
import sys
import os.path
import checkupdate

def cleanup(s):
    s=s.replace("[","")
    s=s.replace("]","")
    s=s.replace("\\","\\\\")
    s=s.replace("Field","")
    s=s.replace('"',"")
    return s

def dotfile2psfile(dotfile,psfile=None,loadall=False):
    if psfile==None:
        psfile=psFileName(dotfile)
    check=checkupdate.CheckUpdate([dotfile],[psfile])
    if check.check(loadall):
        print "ps file %s is up to date" % psfile
    else:
        import commands
        command='dot -Tps -o%s %s' % (psfile,dotfile)
        status,mess=commands.getstatusoutput(command)
        if status:
            print "failed to create ps file"
            print "(do you have dot and the graphviz package installed?)"
            print mess
            exit()
        print "wrote to ps file %s" % psfile

def dotFileName(file):
    return file+".dot"

def psFileName(psfile):
    root,ext=os.path.splitext(psfile)
    return root+".ps"

def printUsage():
    print "Converts a message into a dotfile"
    print "ready to be displayed as a graph with the dot utility"
    print "(graphviz package)"
    print "Usage:\n%s file" % sys.argv[0]
    print "output goes to file.dot and file.ps"
    exit()

if __name__ == "__main__":
    narg=len(sys.argv)
    if narg!=2:
        printUsage()

    file=sys.argv[1]

    messagefile2dotfile(file,loadall=True)
