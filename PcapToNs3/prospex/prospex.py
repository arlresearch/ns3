import optparse
import sys
import os.path
import serializesessions
import TransitionPrerequisites
import exbar

def main(argv):
    parser = optparse.OptionParser(usage="""
            %prog [options] directory
                Expects to find file sessions.txt in directory
                Will run transition prerequisites algorithm and exbar,
                and produce a state machine.
    """)
    (cmdline_options, args) = parser.parse_args(sys.argv[1:])
    if len(args) != 1:
        parser.print_help()
        sys.exit(1)

    targetdir=args[0]

    sessionsfile=os.path.join(targetdir,"sessions.txt")
    if not os.path.exists(sessionsfile):
        parser.print_help()
        print >> sys.stderr,"Sessions file %s not found!" % sessionsfile
        sys.exit(1)

    statetreefile=os.path.join(targetdir,"statetree")
    serializesessions.sessionFile2stateTree(sessionsfile,statetreefile,False)

    labeledtreefile=os.path.join(targetdir,"labeledtree")
    labeltxtfile=os.path.join(targetdir,"labels.txt")
    TransitionPrerequisites.runTransitionPrerequisites(statetreefile,labeledtreefile,labeltxtfile)
    exbar.run_exbar(targetdir)




if __name__=='__main__':
    main(sys.argv)
