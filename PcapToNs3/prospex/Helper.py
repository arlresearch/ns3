import os
import cPickle
#from constants import *
from string import *
import copy
import popen2
from UserDict import DictMixin

import sys
import traceback

def formatExceptionInfo(maxTBlevel=None):
    cla, exc, trbk = sys.exc_info()
    excName = cla.__name__
    try:
        excArgs = exc.__dict__["args"]
    except KeyError:
        excArgs = "<no args>"
    excTb = traceback.format_tb(trbk, None)
    s="Exception %s\nArgs: %s\nTraceback:\n" % (excName, excArgs)
    for t in excTb:
        s+=t
    return s

class ReadOnlyDict(DictMixin):
    """
      This class is a read only dictionary.
      You can do all the non-modifying operations on it you could do on a dictionary.
      It is really an abstract class (as there is no way to insert data into it)
      Child classes should implement specialised insertion methods.
      Since there are no protected members in python, child classes
      will have to access the private __dict field to do this.
    """
    def __init__(self):
        self.__dict={}

    def keys(self):
        return self.__dict.keys()

    def __getitem__(self,key):
        return self.__dict.__getitem__(key)

    def __contains__(self,key):
        return self.__dict.__contains__(key)

    def __iter__(self):
        return self.__dict.__iter__()

    def iteritems(self):
        return self.__dict.iteritems()

class ShallowReference:
    """
      this is just a wrapper for stuff you do not want
      copied by copy.deepcopy
    """
    def __init__(self,value):
        self.val=value

    def __deepcopy__(self, memo):
        return copy.copy(self)


class Helper:
    """
      General class with helper functions etc.
    """

    def __init__(self):
        pass

    def intervalsToList(intervals):
        """
          From a list of intervals, create a list of tokens inside these intervals. e.g. "[(1,3),(6,7)]" returns
          "[1,2,3,6,7]"
        """
        result=[]
        for i in intervals:
            result.extend(range(i[0],i[1]+1))
        result=set(result) # kill duplicates
        return list(result)

    intervalsToList=staticmethod(intervalsToList) # make it a static class method

    def returnIntervals(labels):
        """
          From a list of labels like [0,1,5,6,7] return a list of the uninterrupted intervals as tuples,
          e.g. [(0,1),(5,7)]
          Works on any sequence, and does not assume sequence is sorted
        """
        labelList=sorted(list(labels))
        result=[]
        if labelList==[]:
            return []
        lastStart=labelList[0]
        oldL=labelList[0]
        for l in labelList[1:]:
            if (l==None or oldL==None):
                lastStart=l
                oldL=l
                continue
            if l!=oldL+1:
                result.append((lastStart,oldL))
                lastStart=l
            oldL=l
        result.append((lastStart,labelList[-1]))
        return result

    returnIntervals=staticmethod(returnIntervals) # make it a static class method

    def consecutive(aSet):
        return len(Helper.returnIntervals(aSet))==1

    consecutive=staticmethod(consecutive)

    def splitNonConsecutive(aSet):
        """
            just a helper to split a set of numbers
            into (a list of) subsets of consecutive values
        """
        intervals=Helper.returnIntervals(aSet)
        return [set(range(b,e+1)) for b,e in intervals]

    splitNonConsecutive=staticmethod(splitNonConsecutive) # make it a static class method

    def getFileCount(path,prefix,postfix):
        for root, dirs, files in os.walk(path):
            return len(files)
        return 0

    getFileCount=staticmethod(getFileCount) # make it a static class method

    def stripLabelBrackets(l):
        if l and (l[0]=='{' or l[0]==' '):
            l=l[1:] # remove brackets
        if l and l[-1]=='}':
            l=l[:-1]
        return l
    stripLabelBrackets=staticmethod(stripLabelBrackets) # make it a static class method

    # return object or None if file not found
    def importSAD(filename,count):
        #dir=Const.ALIGNMENT_PATH_PREFIX+filename
        fullname=filename+"."+str(count)+".message"
        print "reading %s" % fullname
        if not os.access(fullname,os.F_OK):
            return None
        res = cPickle.load(open(fullname))
        return res
    importSAD=staticmethod(importSAD)

    def exportSAD(sad,filename):
        dir=Const.ALIGNMENT_PATH_PREFIX+filename
        if not os.access(dir,os.F_OK):
            os.makedirs(dir) # create dir
        # now get max number of existing files
        count=Helper.getFileCount(dir,"","") # current number of files
        cPickle.dump(sad,open(dir+"/"+"part"+"_"+str(count)+".p",'w')) # add new file
    exportSAD=staticmethod(exportSAD)

    def readDump(filename,count):
        dir=Const.MERGE_PATH_PREFIX+filename
        fullname=dir+"/"+"dump"+str(count)
        if not os.access(fullname,os.F_OK):
            return None
        f=open(fullname)
        s=f.read()
        return s
    readDump=staticmethod(readDump)

    def readStringsFile(fullname):
        """
          Read 'strings' output file and return a list of the result tokens
          fullname includes the file's directory
        """
        if not os.access(fullname,os.F_OK):
            print "Cant access %s" % fullname
            return []
        f=open(fullname)
        l=f.readlines()
        result=[]
        for t in l:
            result.append(t[0:-1]) # strip the line terminator
        print "Strings read from %s" % fullname
        return result
    readStringsFile=staticmethod(readStringsFile)

    def inStringsCaseInsensitive(s,strings):
        for ele in strings:
            if s==ele:
                return True
            if upper(s)==ele:
                return True
            if lower(s)==ele:
                return True
        return False
    inStringsCaseInsensitive=staticmethod(inStringsCaseInsensitive)

    def grepInBinary(fullname,searchString):
        if not os.access(fullname,os.F_OK):
            raise Exception("Helper.grepInBinary: binary file %s not found" % fullname)
        # do a primitive search instead of using the shell
        # grep couldnt handle binary search values correctly (or I sucked :-p )
        f=open(fullname)
        lines=f.readlines()
        for l in lines:
            if searchString in l:
                return True
        f.close()

        #r, w, e = popen2.popen3('grep -c "'+searchString+'" '+fullname)
        #res=r.readlines()
        #print e.readlines()
        #r.close()
        #e.close()
        #w.close()
        #if res==['0\n'] or res==[]:
        #  return False
        #else:
        #  return True
    grepInBinary=staticmethod(grepInBinary)
