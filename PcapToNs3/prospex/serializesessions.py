from StateMachine import *
import TransitionPrerequisites
import sys
import checkupdate
import cPickle
import os.path

def stateTree2sessionFile(treefile,sessionfile,loadall):
    check=checkupdate.CheckUpdate([treefile],[sessionfile])
    if check.check(loadall):
        print "session file %s is up to date" % sessionfile
        return
    tree=cPickle.load(open(treefile))
    start=tree.states[0]
    TransitionPrerequisites.addUplinks(start)

    leaves=[s for s in TransitionPrerequisites.traverseStates(start) if len(s.transitions)==0]
    print "Got leaves %s" % str([s.id for s in leaves])

    sessions=TransitionPrerequisites.findSequences(start,leaves)

    saveout = sys.stdout
    fsock = open(sessionfile, 'w')
    sys.stdout = fsock

    for session in sessions:
        for event in session:
            print "%d " %  event,
        print

    sys.stdout = saveout
    fsock.close()

    print "wrote to session file %s" % sessionfile

def getSessions(sessionsfile):
    sf=open(sessionsfile)
    sessions=[line.split() for line in sf]
    sessions=[[int(e) for e in s] for s in sessions]
    return sessions

def sessionFile2stateTree(sessionfile,treefile,loadall):
    check=checkupdate.CheckUpdate([sessionfile],[treefile])
    if check.check(loadall):
        print "tree file %s is up to date" % treefile
        return
    sm=StateMachine()
    sm.newState()
    sessions=getSessions(sessionfile)
    print "sessions %s" % str(sessions)
    for s in sessions:
        sm.addSession(s)

    cPickle.dump(sm,open(treefile,"w"))

    print "wrote to tree file %s" % treefile
    stateMachine2DotFile(treefile)
    stateMachine2TxtFile(treefile)


def printUsage():
    print "Usage:"
    print "%s write statemachinefile sessionsfile" % sys.argv[0]
    print "%s read sessionfile statemachinefile" % sys.argv[0]
    exit()

if __name__ == "__main__":
    narg=len(sys.argv)
    if narg!=4:
        printUsage()

    if sys.argv[1]=="write":
        stateTree2sessionFile(sys.argv[2],sys.argv[3],True)
    elif sys.argv[1]=="read":
        sessionFile2stateTree(sys.argv[2],sys.argv[3],True)
    else:
        printUsage()
