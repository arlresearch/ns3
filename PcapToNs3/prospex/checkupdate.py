import os.path
import sys

class CheckUpdate:
    def __init__(self,infiles,outfiles,optionalinfiles=None):
        self.infiles=infiles
        self.outfiles=outfiles
        self.optionalinfiles=optionalinfiles
        if self.optionalinfiles is None:
            self.optionalinfiles=[]

    def checkInputExists(self):
        filethere=[os.path.exists(file) for file in self.infiles]
        if False in set(filethere):
            missing=self.infiles[filethere.index(False)]
            return False,missing
        return True,None

    def checkOutputExists(self):
        filethere=[os.path.exists(file) for file in self.outfiles]
        if False in filethere:
            missing=self.outfiles[filethere.index(False)]
            return False,missing
        return True,None

    def checkUp2Date(self):
        optionals=filter(os.path.exists,self.optionalinfiles)
        inputs=self.infiles+optionals
        intime=max([os.path.getmtime(f) for f in inputs])
        outtime=min([os.path.getmtime(f) for f in self.outfiles])
        if outtime>=intime:
            #it's up to date already
            return True
        return False

    def check(self,loadall=False):
        exists,missing=self.checkInputExists()
        if not exists:
            print "File %s not found" % missing
            raise IOError
        if loadall:
            return False
        exists,missing=self.checkOutputExists()
        if exists and self.checkUp2Date():
            return True
        return False
