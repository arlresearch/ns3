from StateMachine import *
import numpy
import copy
from Helper import ReadOnlyDict

class Prereq:
    """
     abstract base class for transition prerequisites
    """
    def test(self,startstate,state):
        """
          check if state fulfills prerequisites in the state tree starting at startstate
          return True if it does, False otherwise
        """
        print "test not implemented"
        print self.__class__.__name__
        raise NotImplementedError

    def __str__(self):
        print "__str__ not implemented"
        print self.__class__.__name__
        raise NotImplementedError

class AllowedPrereq(Prereq):
    def __init__(self,allowed):
        self.allowed=allowed#set of allowed events

    def test(self,startstate,state):
        s=set(findSequence(startstate,state))
        return len(s-self.allowed)==0

    def __str__(self):
        return "Allow only: "+str(list(self.allowed))


def findLastOccurrence(startstate,state,events):
    """
      returns target state of transition, or None if no occurrence
    """
    curr=state
    while curr!=startstate:
        event,parent=curr.parenttransition
        if event in events: return curr
        curr=parent
    return None

class BasicPrereq(Prereq):
    def __init__(self,required,allowed):
        #this is a set, 1 of these is required
        self.required=required
        #only these are allowed after LAST occurrence of a required event
        self.allowed=AllowedPrereq(allowed)

    def test(self,startstate,state):
        after=findLastOccurrence(startstate,state,self.required)
        if after is None: return False
        return self.allowed.test(after,state)

    def __str__(self):
        return "Require: %s, then %s" % (str(list(self.required)),str(self.allowed))

class AndPrereq(Prereq):
    def __init__(self):
        self.prereqs=set()

    def test(self,startstate,state):
        for p in self.prereqs:
            if p.test(startstate,state) is False: return False
        return True

    def __str__(self):
        s="AND of:\n"
        for p in self.prereqs:
            s+="\t%s\n" % str(p)
        s+="end AND\n\n"
        return s

class PrereqDict(dict):
    """
      event->prereq dictionary
    """
    def __missing__(self,key):
        r=AndPrereq()
        self[key]=r
        return r

def SHSHelper(sets,maxsize):
    all=reduce(lambda x,y:x|y,sets,set())
    if not all:
        return set()
    if maxsize<=0:
        return None
    for e in all:
        solution=set()
        solution.add(e)
        ss=filter(lambda x:len(x&solution)==0,sets)
        r=SHSHelper(ss,maxsize-1)
        if r is not None:
            solution|=r
            return solution
    return None

def SetHittingSubset(sets,maxsize):
    """
      set hitting problem is dual to set cover:
      find smallest set of elements that "hits" each set
      (has non-empty intersection with each set)

      This is a dumb, brute force, exponential-time implementation,
      but it stops (returning None) if it does not find a subset of size maxsize or less
      (so computational cost is N^maxsize.. keep maxsize small!)
    """
    intersection=reduce(lambda x,y:x&y,sets)
    if intersection:
        #size 1!
        return intersection[0]

    for size in range(2,maxsize+1):
        solution=SHSHelper(sets,size)
        if solution is not None:
            return solution

    return None

def traverseStates(state):
    """
      state machine better be  a tree
    """
    yield state
    for event,targetstate in state.transitions.iteritems():
        for child in traverseStates(targetstate):
            yield child

def traverseTransitions(state):
    """
      state machine better be  a tree
      traverse tree returning all transitions as
       (event,startstate,endstate)
    """
    for state in traverseStates(state):
        for event,targetstate in state.transitions.iteritems():
            yield event,state,targetstate

def addUplinks(state):
    """
      state machine better be a tree
      adds parenttransition=(event,parent) to each state
    """
    for event,start,end in traverseTransitions(state):
        end.parenttransition=(event,start)

def printLabels(startstate):
    for state in traverseStates(startstate):
        print "state %d label:" % state.id
        print str(state.label)

def findPrerequisites(stateTree):
    """
      state machine better be a tree
    """
    startstateid=0
    startstate=stateTree.states[startstateid]
    assert startstate.isStart,"state 0 should be a start state"
    addUplinks(startstate)

    prereqdict=PrereqDict()

    for state in traverseStates(startstate):
        state.final=False

    #now find all occurrences of each event in state machine
    events={}
    for event,start,end in traverseTransitions(startstate):
        t=events.get(event,set())
        t.add((start,end))
        events[event]=t

    for event in events:
        prereqdict[event]

    #now mark final states
    for event,transitions in events.iteritems():
        after=set([t[1] for t in transitions])
        notfinal=filter(lambda s:len(s.transitions)!=0,after)
        if len(notfinal)==0:
            if __name__ == "__main__":
                print "Got a final transition: %s" % str(event)
            for s in after:
                s.final=True

    #find required prereqs for each event
    required2events={}
    for event,transitions in events.iteritems():
        before=set([t[0] for t in transitions])
        seqs=findSequences(startstate,before)
        #
        required=findRequired(startstate,seqs)
        if __name__=="__main__":
            print "looking for requirements for event %s" % str(event)
            print "requires: %s" % str(required)
        for r in required:
            r=frozenset(r)
            old=required2events.get(r,set())
            old.add(event)
            required2events[r]=old
    #print required2events

    #for each requirement, see what is allowed
    for r,eventgroup in required2events.iteritems():
        before=set()
        #get all instances of events in group
        for event in eventgroup:
            transitions=events[event]
            #we are interested in start state of each transition
            b=map(lambda t:t[0],transitions)
            before|=set(b)

        if __name__ == "__main__":
            print "before %s " % str(map(lambda s:s.id,before))
        seqs=findSequences(startstate,before)
        allowed=findAllowed(startstate,r,seqs)
        #now build prerequisite
        required=r
        prereq=BasicPrereq(required,allowed)

        if __name__=="__main__":
            print "Prerequisite %s for group %s:" % (str(r),str(sorted(list(eventgroup))))
            print str(prereq)

        for event in eventgroup:
            prereqdict[event].prereqs.add(prereq)
        #for state in before:
        #  state.prereqs.prereqs.add(prereq)

    #print prereqdict
    return prereqdict

def findSequence(startstate,state):
    seq=[]
    curr=state
    while curr!=startstate:
        event,parent=curr.parenttransition
        #insert at start of list
        seq.insert(0,event)
        curr=parent
    return seq

def findSequences(startstate,states):
    """
      get paths from startstate to states
    """
    sequences=set()
    for state in states:
        #print "prereqs of state %d" % state.id
        seq=findSequence(startstate,state)
        #print seq
        sequences.add(tuple(seq))
    return sequences

def setindex(aseq,aset):
    """
      find index of first element from set in seq
    """
    for i in range(len(aseq)):
        if aseq[i] in aset:
            return i
    raise ValueError

def findAllowed(startstate,eventset,sequences):
    """
      get set of all events after last occurrence of events in eventset
      in sequences
    """
    allowed=set()
    for sequence in sequences:
            #search for event from end of sequence
        tail=list(sequence)
        tail.reverse()
        tail=tail[:setindex(tail,eventset)]
        allowed|=set(tail)
    return allowed

def findRequired(startstate,sequences):
    """
      get set of all events common to all sequences
    """
    if __name__ == "__main__":
        print sequences
    commonsubset=set(reduce(lambda a,b:set(list(a))&set(list(b)),sequences))
    #now do the set cover thing:
    alreadyreq=copy.copy(commonsubset)
    seqs=sequences
    result=set()
    for r in commonsubset:
        s=set()
        s.add(r)
        result.add(frozenset(s))
    while True:
        #remove alreadyreq from sequences
        seqs=map(lambda x:set(filter(lambda y:y not in alreadyreq,x)),sequences)
        if min([len(x) for x in seqs])==0:
            if __name__ == "__main__":
                print "no set needed"
            break
        if __name__ == "__main__":
            print "reduced to %s" % str(seqs)
        r=SetHittingSubset(seqs,5)
        if not r:
            break
        if __name__ == "__main__":
            print "found set hitting subset: %s" % str(r)
        alreadyreq|=r
        result.add(frozenset(r))

    return result

def setLabels(tree,prereqdict):
    startstateid=0
    startstate=tree.states[startstateid]

    #add an empty label set to each state
    for state in traverseStates(startstate):
        state.label=set()

    for state in traverseStates(startstate):
        if state.id==0:
            pass
        if state.final:
            #no events are allowed in a final state!
            continue
        for event,prereq in prereqdict.iteritems():
            if prereq.test(startstate,state):
                state.label.add(event)
    if __name__=="__main__":
        printLabels(startstate)

def matrixToTxtFile(m,file):
    saveout = sys.stdout
    fsock = open(file, 'w')
    sys.stdout = fsock
    rows=m.shape[0]
    cols=m.shape[1]
    assert rows==cols,"want square matrix"
    #print in format that boost::ublas can read, such as
    #[3,3]((0,1,0),(1,0,1),(0,1,0))
    s= "[%s,%s](" %(rows,rows)
    for i in range(0,rows):
        s+= "(%s" % str(m[i][0])
        for j in range(1,rows):
            s+= ",%s" % str(m[i][j])
        s+= ")"
        if i!=rows-1:
            s+= ","
    s+= ")\n"
    print s
    sys.stdout = saveout
    fsock.close()

def computeLabels(treefile,labeledtreefile,loadall=False):
    check=checkupdate.CheckUpdate([treefile],[labeledtreefile])
    if check.check(loadall):
        print "labeled state tree file '%s' is up to date" % labeledtreefile
        return
    print "state tree file '%s' being loaded" % treefile
    tree=cPickle.load(open(treefile))
    prereqdict=findPrerequisites(tree)
    setLabels(tree,prereqdict)
    cPickle.dump(tree,open(labeledtreefile,"w"))
    print "wrote to labeled tree file '%s'" % labeledtreefile

def labeledTree2labelsTxt(labeledtreefile,labeltxtfile,loadall=False):
    check=checkupdate.CheckUpdate([labeledtreefile],[labeltxtfile])
    if check.check(loadall):
        print "label txt file '%s' is up to date" % labeltxtfile
        return
    print "labeled state tree file '%s' being loaded" % labeledtreefile
    tree=cPickle.load(open(labeledtreefile))
    tree.toLabelsFile(labeltxtfile)
    print "wrote to label txt file '%s'" % labeltxtfile

def runTransitionPrerequisites(statetreefile,labeledtreefile,labeltxtfile):
    computeLabels(statetreefile,labeledtreefile,True)
    stateMachine2DotFile(labeledtreefile,None,True)
    labeledTree2labelsTxt(labeledtreefile,labeltxtfile,True)

def printUsage():
    print "Usage:\n%s statetreefile labeldtreefile labeltxtfile" % sys.argv[0]
    exit()



if __name__ == "__main__":
    narg=len(sys.argv)
    if narg!=4:
        printUsage()

    runTransitionPrerequisites(sys.argv[1],sys.argv[2],sys.argv[3])
