import checkupdate
import cPickle
import commands
import subprocess
import os
import signal
import sys
import StateMachine

sub=None
limit=None
killed=False

def handler(one,two):
    print "Exbar took longer than time limit of %d seconds: KILL IT!" % limit
    #print "killing pid %d, signal %d" % (sub.pid,signal.SIGKILL)
    #this does not work! sub.pid gives me my pid instead of the child's
    #os.kill(sub.pid,signal.SIGKILL)
    #so use killall
    command="killall exbar"
    status,message = commands.getstatusoutput(command)
    if status:
        print >>sys.stderr, "ACK! PROCESS NOT KILLED?"
    global killed
    killed=True


def exbar(treetxtfile,labelsfile,statemachinetxtfile,statemachinelabelsfile,time_limit,max_states,loadall=False):
    check=checkupdate.CheckUpdate([treetxtfile,labelsfile],[statemachinetxtfile,statemachinelabelsfile])
    if check.check(loadall):
        print "state machine txt file %s is up to date" % statemachinetxtfile
        return True

    command="./exbar %s %s %d %s %s" % (treetxtfile,labelsfile,max_states,statemachinetxtfile,statemachinelabelsfile)
    print "running exbar! (%s)" % command
    global sub
    global limit
    global killed
    limit=time_limit
    sub=subprocess.Popen(command,shell=True)

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(time_limit)
    exbarout,exbarerr=sub.communicate()
    signal.alarm(0)
    status=sub.poll()
    if status:
        #if status==-signal.SIGALRM:
        if killed:
            #we killed it, it's ok
            return False
        print "failed to run exbar"
        if exbarerr: print exbarerr
        exit(1)

    print "wrote state machine to txt file %s" % statemachinetxtfile
    return True

#time limit for exbar, in seconds
exbar_time_limit=300

#maximum number of states in final state machine
exbar_max_states=20

def run_exbar(targetdir):
    treetxtfile=os.path.join(targetdir,"statetree.txt")
    labelstxtfile=os.path.join(targetdir,"labels.txt")
    statemachinetxtfile=os.path.join(targetdir,"statemachine.txt")
    statemachinefile=os.path.join(targetdir,"statemachine")
    labeledstatemachinefile=os.path.join(targetdir,"labeledstatemachine")
    statemachinelabelsfile=os.path.join(targetdir,"statemachine-labels.txt")
    successful=exbar(treetxtfile,labelstxtfile,statemachinetxtfile,statemachinelabelsfile,exbar_time_limit,exbar_max_states)
    loadall=False
    sessionsfile=os.path.join(targetdir,"sessions.txt")
    sm=StateMachine.stateMachineFromTxtFile(statemachinetxtfile,statemachinefile,sessionsfile,loadall)
    StateMachine.stateMachine2DotFile(statemachinefile,None,loadall)
    sm.loadLabels(statemachinelabelsfile)
    cPickle.dump(sm,open(labeledstatemachinefile,"w"))
    StateMachine.stateMachine2DotFile(labeledstatemachinefile,None,loadall)


if __name__ == "__main__":
    parser = optparse.OptionParser(usage="""%prog directory""")
    (cmdline_options, args) = parser.parse_args(sys.argv[1:])
    if len(args) != 1:
        parser.print_help()
        sys.exit(1)

    targetdir=args[0]
    run_exbar(targetdir)
