import sys
import checkupdate
import cPickle
from message2dotfile import dotfile2psfile
import os.path
import random
import serializesessions

class State:
    def __init__(self,id,isStart=False):
        self.id=id
        self.transitions={}
        self.isStart=isStart

    def addTransition(self,event,targetstate):
        assert event not in self.transitions,"state %d already has a transition for event %d" % (self.id,event)
        self.transitions[event]=targetstate

class StateMachine:
    def __init__(self):
        self.states={}

    def newState(self):
        if len(self.states)==0: id=0
        else: id=max(self.states.keys())+1
        s=State(id,id==0)
        self.states[id]=s
        return s

    def toDotFile(self,dotfile):
        saveout = sys.stdout
        fsock = open(dotfile, 'w')
        sys.stdout = fsock

        print "digraph message{"
        for id,state in self.states.iteritems():
            if hasattr(state,"label"):
                label=str(list(state.label))
            elif hasattr(state,"labelstring"):
                label=state.labelstring
            else:
                label=str(id)
            print 'node%d [label="%s"' % (id,label),
            if state.isStart:
                print ',shape=doublecircle',
            elif len(state.transitions)==0:
                #end state
                print ',shape=circle,style=bold',
            else:
                #default shape is circle
                print ',shape=circle',
            print '];'

        for state in self.states.itervalues():
            for event,targetstate in state.transitions.iteritems():
                print 'node%d -> node%d [label="%s"];' % (state.id,targetstate.id,str(event))

        print "}"

        sys.stdout = saveout
        fsock.close()

    def toLabelsFile(self,labelstxtfile):
        saveout = sys.stdout
        fsock = open(labelstxtfile, 'w')
        sys.stdout = fsock

        for id,state in self.states.iteritems():
            assert hasattr(state,"label"),"no label attribute to print out!"
            labels=sorted(list(state.label))
            print "%d:\t" % id,
            for l in labels:
                print l,

            print

        sys.stdout = saveout
        fsock.close()

    def addSession(self,events):
        currstate=self.states[0]
        for event in events:
            if event in currstate.transitions:
                targetstate=currstate.transitions[event]
            else:
                targetstate=self.newState()
                currstate.transitions[event]=targetstate
            currstate=targetstate

    def delSession(self,events):
        currstate=self.states[0]
        for event in events:
            if event in currstate.transitions:
                if event==events[-1] and currstate.transitions[event].transitions=={}:
                    del currstate.transitions[event] # delete transition
                    del self.states[event]
                    self.delSession(events[:-1])
                    return
                targetstate=currstate.transitions[event]
            else:
                print "Event not found, aborting"
                sys.exit(1)
            currstate=targetstate

    def randomSession(self,length,weighted=False):
        assert hasattr(self,"eventCount") or not weighted,"can't do weighted random sessions when i don't have weights"
        session=[]
        curr=self.states[0]
        while length>0:
            if len(curr.transitions)==0: break

            #possible events:
            possible=list(curr.transitions.keys())

            if weighted:
                #replicate events based on weight
                wp=[]
                for e in possible:
                    w=self.eventCount[(curr.id,e)]
                    wp.extend([e]*w)
                possible=wp

            e=random.choice(possible)
            session.append(e)
            curr=curr.transitions[e]
            length-=1
        return session

    def parseSession(self,session):
        curr=self.states[0]
        for e in session:
            #print "in state %d, got event %d" % (curr.id,e)
            if e not in curr.transitions:
                #print "fail!"
                return False
            curr=curr.transitions[e]
        return True

    def toTxtFile(self,txtfile):
        """
          print it as a txt file where each line is

          event    startstateid    endstateid
        """
        saveout = sys.stdout
        fsock = open(txtfile, 'w')
        sys.stdout = fsock

        for state in self.states.itervalues():
            for event,targetstate in state.transitions.iteritems():
                print '%s\t%d\t%d' % (str(event),state.id,targetstate.id)

        sys.stdout = saveout
        fsock.close()

    def fromTxtFile(self,txtfile):
        tf=open(txtfile)
        for line in tf:
            nn=[int(word) for word in line.split()]
            assert len(nn)==3,"invalid input line: "+str(nn)
            event,start,target=nn
            if start not in self.states:
                self.states[start]=State(start,start==0)
            if target not in self.states:
                self.states[target]=State(target,target==0)
            self.states[start].addTransition(event,self.states[target])

    def loadLabels(self,labelsfile):
        f=open(labelsfile)
        for line in f:
            l=line.split(":",1)
            if len(l)!=2:
                print "Error parsing labelsfile %s" % labelsfile
                print "expected ':' in line: '%s'" % line
                exit(1)
            id=int(l[0])
            label=l[1].strip()
            if id not in self.states:
                print "Error parsing labelsfile %s" % labelsfile
                print "state %d not found" % id
                exit(1)
            self.states[id].labelstring=label

    def loadCounts(self,sessionsfile):
        sessions=serializesessions.getSessions(sessionsfile)
        #(stateid,eventid): count
        self.eventCount={}
        for s in sessions:
            curr=self.states[0]
            for e in s:
                assert e in curr.transitions,"StateMachine.loadCount: this session doesn't even parse!"
                key=(curr.id,e)
                val=self.eventCount.get(key,0)
                self.eventCount[key]=val+1
                curr=curr.transitions[e]



def stateMachine2DotFile(smfile,dotfile=None,loadall=False):
    if dotfile is None: dotfile=smfile+".dot"
    check=checkupdate.CheckUpdate([smfile],[dotfile])
    if check.check(loadall):
        print "state machine dot file %s is up to date" % dotfile
    else:
        sm=cPickle.load(open(smfile))
        sm.toDotFile(dotfile)
        print "wrote to state machine dot file %s" % dotfile

    dotfile2psfile(dotfile,None,loadall)

def stateMachine2TxtFile(smfile,txtfile=None,loadall=False):
    if txtfile is None: txtfile=smfile+".txt"
    check=checkupdate.CheckUpdate([smfile],[txtfile])
    if check.check(loadall):
        print "state machine txt file %s is up to date" % txtfile
    else:
        sm=cPickle.load(open(smfile))
        sm.toTxtFile(txtfile)
        print "wrote to state machine txt file %s" % txtfile

def stateMachineFromTxtFile(txtfile,smfile,sessionsfile=None,loadall=False):
    check=checkupdate.CheckUpdate([txtfile],[smfile])
    if check.check(loadall):
        print "state machine file %s is up to date" % smfile
        return cPickle.load(open(smfile))
    else:
        sm=StateMachine()
        sm.fromTxtFile(txtfile)
        if sessionsfile is not None:
            sm.loadCounts(sessionsfile)
        cPickle.dump(sm,open(smfile,'w'))
        print "wrote to state machine pickle file %s" % smfile
        return sm

if __name__ == "__main__":
    print "this module does nothing standalone. Use serializestatemachine for reading state machines txt files"
