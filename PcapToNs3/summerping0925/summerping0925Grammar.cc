
#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>

using namespace std;

enum stateName {node0 = 0, node42 = 1};
class Transition{
        int transNum;
        stateName state;

public:
        void addState(stateName newState){
                state = newState;
                }
        void addTransNum(int nextNum){
                transNum = nextNum;

                }
        stateName getName(){
                return state;
                }
        int getNum(){
                return transNum;
                }

        };



class State{

public:

        vector<Transition> trans;
        void addTrans(int input,stateName stateNumber){
                Transition temp;
                temp.addState(stateNumber);
                temp.addTransNum(input);
                trans.push_back(temp);
                }

        };


class StateMachine{
        State stateArray[2];
        int currState = 0;

public:
        StateMachine(){
        stateArray[node0].addTrans(0, node42);
        stateArray[node42].addTrans(1, node0);
        
                }

        vector<int> getNextState(int input){
                int found = 0;
                printf("Input is:%i ", input);
                int transitNum;
                vector<int> result;
                //printf("Current size is: %i", stateArray[currState].trans.size());
                for(int i = 0; i< stateArray[currState].trans.size(); i++){
                        stateName state = stateArray[currState].trans[i].getName();
                        transitNum = stateArray[currState].trans[i].getNum();
                        //printf("%s%i","trans is: ",transitNum);
                        if(transitNum == input){
                                //result.push_back(transitNum);
                                currState = state;
                                //printf("CurrState changed: %i", currState);
                                found = 1;
                                break;
                        }
                }
                if(found){
                        for(int i = 0; i< stateArray[currState].trans.size(); i++){
                                transitNum = stateArray[currState].trans[i].getNum();
                                result.push_back(transitNum);
                        //printf("%s%i","trans is: ",transitNum);
                        }
                        //printf("%s%i","trans is: ",transitNum);
                }
                else{
                //if(result.size()==0){
                        result.push_back(-1);
                        //}
                        }
                 return result;
                }
};

void printV(vector<int> out){
        if(out.size()==1){
                printf("transition is: ");
        }
        else{
                printf("transitions are: ");
        }
        for(int i = 0; i < out.size(); i++){
                printf("%i ",out[i]);
        }
        printf("\n");

}

