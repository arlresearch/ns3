//This file was generated using xmlToNs4Scenario.py, please do not edit unless you know exactly what you're doing.
#include <fstream>
#include <string>
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/summerping0925.h"
#include "ns3/summerping0925-helper.h"

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("Input summerping0925");

int main(int argc, char** argv){
        GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));
        NS_LOG_INFO("Creating Nodes");
	Ptr<Node> n0 = CreateObject<Node>();
	Ptr<Node> n1 = CreateObject<Node>();
 
      NodeContainer nodes(n0,n1);

    NS_LOG_INFO ("Create IPv4 Internet Stack");
        InternetStackHelper internetv4;
        internetv4.Install(nodes);

        NS_LOG_INFO("Create channels.");
        CsmaHelper csma;
        csma.SetChannelAttribute("DataRate", DataRateValue (50000000));
        csma.SetChannelAttribute("Delay", TimeValue (MilliSeconds(100)));
        NetDeviceContainer devices = csma.Install(nodes);
        csma.EnablePcapAll("allCapture", true);

        
    Ipv4AddressHelper ipv4;
        ipv4.SetBase("0.0.0.0", "1.0.0.0");
        Ipv4InterfaceContainer ipv4IntC;
 	

	Ipv4Address ip1("10.0.2.15");
	ipv4.AssignIpToithNetDevice(devices,ipv4IntC,0, ip1);
	Ipv4Address ip2("216.58.216.14");
	ipv4.AssignIpToithNetDevice(devices,ipv4IntC,1, ip2);

        summerping0925Helper summerping0925HelperHelper = summerping0925Helper(ip2);
    summerping0925HelperHelper.SetAttribute("Verbose", BooleanValue(true));
    summerping0925HelperHelper.SetAttribute("Interval", TimeValue(Seconds(1)));
    ApplicationContainer apps = summerping0925HelperHelper.Install(nodes.Get(0));

    apps.Start(Seconds (4.0));
    apps.Stop(Seconds (20.0));

    NS_LOG_INFO("Run Simulation");
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Done...");
}